package ch.quantasy.ch.quantasy.mqtt.tutorial.step06.service;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.time.Instant;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 *
 * @author Reto E. Koenig <reto.koenig@bfh.ch>
 */
public class TimeService {

    public static final String PROTOCOL = "tcp";
    public static final String BROKER = "localhost";
    public static final String PORT = "1883";
    public static final URI SERVER_URI;
    private static final String CLIENT_ID;

    public static final String TIME_SERVICE_TIME_10_TOPIC;
    public static final String TIME_SERVICE_TIME_3_TOPIC;
    public static final String TIME_SERVICE_LASTWILL_TOPIC;

    public static final String SERVICE_TYPE = "TimeService";
    public static final String SERVICE_INSTANCE = "step06";

    static {
        SERVER_URI = URI.create(PROTOCOL + "://" + BROKER + ":" + PORT);
        CLIENT_ID = "ch.quantasy.mqtt.tutorial.TimeService.step06";

        TIME_SERVICE_TIME_10_TOPIC = SERVICE_TYPE + "/" + SERVICE_INSTANCE + "/" + "delay/10/time";
        TIME_SERVICE_TIME_3_TOPIC = SERVICE_TYPE + "/" + SERVICE_INSTANCE + "/" + "delay/3/time";
        TIME_SERVICE_LASTWILL_TOPIC = SERVICE_TYPE + "/" + SERVICE_INSTANCE + "/" + "testament";

    }

    private final MqttClient mqttClient;

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> ticker10;
    private ScheduledFuture<?> ticker3;

    public TimeService() throws MqttException {
        mqttClient = new MqttClient(SERVER_URI.toString(), CLIENT_ID, new MemoryPersistence());
    }

    public void connectToBroker() throws MqttException {
        MqttConnectOptions connectOptions = new MqttConnectOptions();
        connectOptions.setWill(TIME_SERVICE_LASTWILL_TOPIC, "offline".getBytes(), 1, true);
        mqttClient.connect(connectOptions);
        mqttClient.publish(TIME_SERVICE_LASTWILL_TOPIC, "online".getBytes(), 1, true);
    }

    public void disconnectFromBroker() throws MqttException {
        mqttClient.disconnect();
    }

    public void startPublishingTicks() {
        if (ticker10 != null) {
            return;
        }
        ticker10 = scheduler.scheduleAtFixedRate(new TimeTickManager(TIME_SERVICE_TIME_10_TOPIC), 10, 10, TimeUnit.SECONDS);
        ticker3 = scheduler.scheduleAtFixedRate(new TimeTickManager(TIME_SERVICE_TIME_3_TOPIC), 3, 3, TimeUnit.SECONDS);
    }

    public void stopPublishingTicks() {
        if (ticker10 != null) {
            ticker10.cancel(true);
            ticker10 = null;
        }
        if (ticker3 != null) {
            ticker3.cancel(true);
            ticker3 = null;
        }
    }

    class TimeTickManager implements Runnable {

        private final String topic;

        public TimeTickManager(String topic) {
            this.topic = topic;
        }

        @Override
        public void run() {
            try {
                Instant now = Instant.now();
                String nowString = now.toString();
                byte[] nowStringAsBytes = nowString.getBytes("UTF-8");
                MqttMessage message = new MqttMessage(nowStringAsBytes);
                message.setRetained(true);
                message.setQos(1);
                mqttClient.publish(topic, message);
            } catch (UnsupportedEncodingException | MqttException ex) {
                Logger.getLogger(TimeService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
